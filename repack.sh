#!/bin/bash
#
# Clean up everything then build iso from scratch
set -e -x
git clean -f
rm -fr initrd isotree kernel
cp save/* . 2>/dev/null
./mkit.sh
./packiso.sh
/old/stuff/git/exfat/HERE/sbin/mount.exfat-fuse /dev/sda1 X
cp tiniest.iso X/
umount X
