#!/bin/bash
#
# QEMU setup for trial VM

ISO=${ISO:-tiniest.iso}

N="12345678"
if [ ${#1} = 1 ] && [ "${N#*$1}" != "$N" ] ; then
    REPLY=$1
else
    VARIANT=(
	"use bootimage.raw as harddisk with legacy boot (and boot menu)"
	"use bootimage.raw as harddisk with UEFI boot"
	"use ${ISO} as harddisk with legacy boot (and boot menu)"
	"use ${ISO} as harddisk with UEFI boot"
	"use ${ISO} as cdrom with legacy boot (and boot menu)"
	"use ${ISO} as cdrom with UEFI boot"
	"use ${ISO} as USB disk with legacy boot (and boot menu)"
	"use ${ISO} as USB disk with UEFI boot"
    )

    select V in "${VARIANT[@]}" ; do
	[ -z "$V" ] || break
    done
fi
[ -z "$REPLY" ] && exit 0

FILE=bootimage.raw
[ $REPLY -ge 3 ] && FILE=${ISO}
MEDIA=disk
[ $REPLY -ge 5 ] && MEDIA=cdrom
[ $REPLY -ge 7 ] && MEDIA=usb

case "$MEDIA" in
    disk)
	DISK2="-drive index=1,format=raw,media=$MEDIA,file=$FILE"
	;;
    usb)
	USB="
        -device qemu-xhci,id=xhci
        -drive if=none,id=stick,format=raw,media=disk,file=${FILE}
        -device usb-storage,bus=xhci.0,port=1,drive=stick
	"
	;;
    cdrom*)
	DISK2="-drive index=1,format=raw,media=$MEDIA,file=$FILE"
	;;
esac

BOOT="-boot menu=on,splash=boot.jpg,splash-time=60000"
[ $(( $REPLY % 2 )) -eq 0 ] &&  BOOT="-bios /usr/share/OVMF/OVMF_CODE.fd"

case "$NET" in
    vde)
	NET="-net nic,model=e1000 -net vde,sock=/run/vde.ctl"
	;;
    *)
	NET=
	;;
esac

[ -e disk.raw ] || dd if=/dev/zero of=disk.raw bs=1G count=0 seek=8

ARGS=(
    -m 2G -M pc,accel=kvm -cpu host -vga qxl
    -serial mon:stdio -echr 0x1c # Using ^\ as meta character
    $BOOT
    -drive index=0,id=disk,media=disk,format=raw,file=disk.raw
    $DISK2
    $USB
    $NET
)
echo "${ARGS[*]}" | sed "s/ -/\n -/g"

qemu-system-x86_64 ${ARGS[@]}
tput reset # EFI boot messes up the terminal

