#!/bin/bash
#
# A script to prepare the tiniest "linux system". It's a kernel and an
# initrd within a FAT with a syslinux boot loader. The initrd contains
# a fully expanded busybox and uses /bin/sh as its init.
#
# This script creates and packs that initrd, as well as the whole,
# boot image. 

set -e

echo '### Step 1. Download original Packages file from the source'
REPOS=( deb.devuan.org/{merged_daedalus_main,merged_ceres_main} )
ARCH="amd64"
MBR=dos # gpt

for REPO in ${REPOS[@]} ; do
    W=( ${REPO//_/ } )
    SUITE=${W[1]}
    SECTION=${W[2]}
    
    PKGFILE=${W[0]/\//_}_${SUITE}_${SECTION}_binary-${ARCH}_Packages
    if [ ! -r $PKGFILE ] ; then
	XZSRC="http://${W[0]}/dists/$SUITE/$SECTION/binary-$ARCH/Packages.xz"
	echo "$XZSRC"
	wget -qO - $XZSRC | xzcat - > ${PKGFILE}
    fi

    echo '# Reduce Packages file into two maps for filename and depends'
    echo "# ..creating mapdepends.txt and mapfile.txt"
    awk '
    BEGIN { print "###" >> "mapdepends.txt"; print "###" >> "mapfile.txt"; }
$1=="Package:" {P=$2; next}
$1=="Pre-Depends:" {print P,$0 >> "mapdepends.txt";next }
$1=="Depends:" {print P,$0 >> "mapdepends.txt";next }
$1=="Filename:" {print P,$2 >> "mapfile.txt";next }
' ${PKGFILE}
done

# Function to find the filename for a given package in the given mapfile
maplookup() {
    local FN=${3:-tail}
    echo "maplookup $1 $2" >&2
    grep "^$1 " $2 >&2
    grep "^$1 " $2 | $FN -n 1 | sed 's/[^ ]* //'
}

# Function to download a deb file and return its name
debfile() {
    local FN=${2-tail}
    local F="$(maplookup $1 mapfile.txt $FN)"
    if [ ! -e "${F##*/}" ] ; then
	for REPO in ${REPOS[@]} ; do
	    echo "download http://${REPO%%_*}/$F" >&2
	    wget -q "http://${REPO%%_*}/$F" && break
	    ls -l $(basename $F) >&2
	done || return 1
    fi
    echo "${F##*/}"
}

# Function to extract from a deb without executing and pre/post scripts
# $1 = rootfs $2 = package
debextract() {
    ar p $2 data.tar.xz | tar xJf - -C $1
}

# Deteremine which kernel to use; this is
echo "# Determine kernel"
KERNEL="$(maplookup linux-image-amd64 mapdepends.txt head | \
		    sed 's/.*\(linux-image[^ ]*\).*/\1/')"
echo "# ... $KERNEL"

echo '### Step 2. Create and populate the initrd, and packit up.'
# The initrd contains only a few kernel modules for coping with a
# later pivoting onto a "full" filesystem.

echo "# Create initrd filesystem"
rm -fr initrd

echo "# Extract busybox, and fluff it up"
mkdir initrd
debextract initrd $(debfile busybox-static head)
for L in $(initrd/bin/busybox --listfull) ; do
    mkdir -p $(dirname initrd/$L)
    case "$L" in
	bin/busybox) : ;;
	usr/*) ln -s ../../bin/busybox initrd/$L ;;
	sbin/*) ln -s ../bin/busybox initrd/$L ;;
	bin/*)  ln -s busybox initrd/$L ;;
	linuxrc) ln -s bin/busybox initrd/$L ;;
    esac
done

echo "# Extract the kernel package ($KERNEL)"
echo "# .. and syslinux stuff if needed"
if [ ! -d kernel ] ; then
    mkdir kernel
    debextract kernel $(debfile $KERNEL head)
    debextract kernel $(debfile syslinux)
    debextract kernel $(debfile syslinux-common)
    debextract kernel $(debfile syslinux-efi)
    debextract kernel $(debfile syslinux-utils)
    debextract kernel $(debfile isolinux)
fi

echo "# Include some kernel modules in the initrd"
MODULES=(
    # disk
    scsi_common scsi_mod libata ata_piix ata_generic cdrom sr_mod
    crc32-pclmul crct10dif_common crc-t10dif crc64 crc64-rocksoft
    t10-pi sd_mod sg
    nls_cp437 nls_ascii fat vfat
    crc32c_generic jbd2 mbcache crc16 ext4
    usb-storage usbcore usb-common xhci-pci xhci-hcd
    isofs exfat loop
    # input
    psmouse evdev
    # network
    e1000
)
MOODLES=""
B=$(pwd)
for m in ${MODULES[@]} ; do
    km=$(find kernel/lib/modules -name $m.ko)
    if [ -z "$km" ] ; then
	echo "Missing module $m"
	continue
    fi
    im=initrd/${km#kernel/}
    MOODLES+=" $B/$im"
    mkdir -p $(dirname $im)
    cp -n $km $im
done
V=${KERNEL#linux-image-}
mkdir -p initrd/boot initrd/lib/modules/$V
cp kernel/boot/System.map-$V initrd/
cp kernel/lib/modules/$V/modules.order initrd/lib/modules/$V/
cp kernel/lib/modules/$V/modules.builtin initrd/lib/modules/$V/
depmod -F initrd/System.map-$V -b initrd $V $MOODLES

echo "# setup a scripted init. The kernel runs this via the #! interpreter"
rm -f initrd/sbin/init # just in case
cp init/init initrd/init
chmod a+x initrd/init

echo "# Declare password-less root"
mkdir -p initrd/etc
echo 'root::0:0:root:/root:/bin/bash' > initrd/etc/passwd

echo "# Now pack up that initrd as initrd.gz"
( cd initrd ; find . | fakeroot cpio -H newc -o | gzip ) >initrd.gz

echo '### Step 3. create a 32  Mb fat filesystem with bios and UEFI boot'
rm -f bootimage.raw
dd if=/dev/zero of=bootimage.raw bs=32M count=1

# Prepare a gpt/dos partition table with a first partition marked as EFI
sfdisk bootimage.raw <<EOF
label: $MBR

2048 32767 U *
- - L
EOF

# Add a fat filesystem at 2048 61440
mkfs.fat -n TINIEST --offset 2048 -F 16 bootimage.raw
IMG="-i bootimage.raw@@$((2048*512))"

# Add an ext2 filesystem at offset 34816*512
# Copy initrd.gz and kernel into the fat filesystem root
EXT=offset=$((34816*512))
mke2fs -t ext4 -E $EXT -F bootimage.raw 15M

mcopy $IMG initrd.gz ::
mcopy $IMG kernel/boot/vm* ::/vmlinuz
mcopy $IMG splash.png ::/

mmd $IMG ::/boot
mmd $IMG ::/boot/syslinux
mmd $IMG ::/boot/syslinux/bios
mcopy $IMG \
      kernel/usr/lib/syslinux/modules/bios/* ::/boot/syslinux/bios
mcopy $IMG syslinux-legacy.cfg ::/syslinux.cfg

syslinux --install --offset=${IMG#*@@} bootimage.raw

mmd $IMG ::/EFI
mmd $IMG ::/EFI/BOOT
mcopy $IMG kernel/usr/lib/SYSLINUX.EFI/efi64/syslinux.efi \
      ::/EFI/BOOT/bootx64.efi
mcopy $IMG \
      kernel/usr/lib/syslinux/modules/efi64/* ::/EFI/BOOT
mcopy $IMG syslinux-uefi.cfg ::/EFI/BOOT/syslinux.cfg
## Add lua boot script
mcopy $IMG muffin.lua ::/EFI/BOOT/muffin.lua

case "$MBR" in
    dos) MBRBIN=mbr.bin ;;
    gpt)
	MBRBIN=gptmbr.bin
	sfdisk --relocate gpt-bak-std bootimage.raw
	sfdisk -f --part-attrs bootimage.raw 1 LegacyBIOSBootable
	;;
    *) exit 1 ;;
esac
dd conv=notrunc of=bootimage.raw bs=440 count=1 \
   if=kernel/usr/lib/syslinux/mbr/$MBRBIN

echo "# populate the extra partition"
if [ -d extra ] ; then
    mkdir -p X
    fuse2fs -o fakeroot -o$EXT bootimage.raw X
    rsync -r extra/. X/.
    umount X
fi

exit
