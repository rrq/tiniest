#!/bin/bash
#
# Run xorriso to make a bootable ISO of bootimage.raw; a hybrid, multi
# mode bootable ISO.

set -e

ISOTREE=isotree
ISO=tiniest.iso
P1=part1.raw # $ISOTREE/part1.raw

echo "# Prepare ISO content tree unless it's already set up"
if [ ! -d $ISOTREE ] ; then
    mkdir $ISOTREE

    # Determine start and size of first parition
    X=( $(sfdisk -l -ostart,sectors bootimage.raw | tail -n 2) )
    dd if=bootimage.raw of=$P1 skip=${X[0]} count=${X[1]}

    echo "# Set up the isolinux boot loader in $ISOTREE"
    mkdir -p $ISOTREE/boot/isolinux/bios
    cp kernel/usr/lib/syslinux/modules/bios/* $ISOTREE/boot/isolinux/bios
    cp kernel/usr/lib/ISOLINUX/isolinux.bin $ISOTREE/boot/isolinux

    ## ldlinux.c32 must be sibling to isolinux.bin
    mv $ISOTREE/boot/isolinux/bios/ldlinux.c32 $ISOTREE/boot/isolinux

    ## use isolinux.cfg for isolinux boot
    cp syslinux-iso.cfg $ISOTREE/boot/isolinux/isolinux.cfg

    ## put splash, vmlinux and initrd as siblings
    mcopy -i $P1 ::/splash.png ::/vmlinuz ::/initrd.gz $ISOTREE/boot/isolinux
    
fi

[ -r $ISO ] && rm $ISO

PREPID="'$(id -un), $(getent passwd $(id -un)|awk -F: '{print $5}')'"

XORRISO=(
    -report_about ALL
    -outdev $ISO
    -map $ISOTREE /
    -rockridge on -joliet on
    -volid "TINIEST"
    -volset_id "$(date +"Tiniest %Y-%m-%d %H:%M:%S")"
    -publisher "Ralph (rrq) Ronnquist"
    -application_id "Hybrid Multi Mode Disk Image"
    -system_id "Tiniest Linux"
    #-copyright_file copyright.txt
    #-abstract_file abstract.txt
    #-biblio_file biblio.txt
    -preparer_id "$PREPID"
    #-application_use path
    #-out_charset "UTF8" ??
    -uid 0 -gid 0

    # Mastering a cdrom legacy boot that uses the ISO tree only
    -boot_image isolinux system_area=kernel/usr/lib/ISOLINUX/isohdpfx.bin

    # Mastering a disk legacy boot reusing the cdrom legacy boot
    -boot_image isolinux dir=/boot/isolinux
    #-boot_image  any iso_mbr_part_type=0x00
    -boot_image isolinux  partition_entry=gpt_basdat
    
    # Mastering a disk UEFI boot using the separate boot partition $P1
    # Bootloader = /EFI/BOOT/bootx64.efi
    -boot_image any next
    -append_partition 2 0xef ${P1}
    -boot_image any 
    efi_path=--interval:appended_partition_2:all::/EFI/BOOT/bootx64.efi

    # make part#2 bootable
)

xorriso "${XORRISO[@]}"
#sfdisk -f --part-attrs tiniest.iso 2 LegacyBIOSBootable
#sfdisk -A tiniest.iso 2
