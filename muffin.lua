local sl = require "syslinux"
local vesa = require "vesa"
local dmi = require "dmi"
local pci = require "pci"

vesa.setmode("800x600")
--vesa.load_background "sample2.jpg"

for c in string.gmatch ("Hello World! - VESA mode", ".") do
    io.write (c)
    sl.msleep(10)
end

io.write("\n")


-- cmdline(ix) = concatenates the command line arguments from
-- the index ix and up, with space separation
function cmdline(ix)
   local line = ""
   while arg[ix] do
      line = line .. " " .. arg[ix]
      ix = ix + 1
   end
   return string.sub(line,2) -- drop the initial space
end

-- dumptable(tbl) = print table entries
function dumptable(name, tbl,vtype)
   print("== begin "..name)
   if tbl then
      for k,v in pairs(tbl) do
	 if vtype == nill or type(v) == vtype then
	    print(k,v)
	 end
      end
   else
      print(nil)
   end
   print( "== end " .. name )
   return nil
end

-- dump_G(k) = print the value of _G[k]
function dump_G(k)
   dumptable(k,_G[k],nil)
   return nil
end

--[[
dumptable("tables",_G,"table")
dumptable("package.loaded",_G["package"]["loaded"], nil)
dumptable("pci.getidlist",pci.getidlist("/pci.ids"), nil)
]]

-- [[
dmitable = dmi.gettable()
dumptable("dmitable",dmitable,nil)
--dumptable("dmitable.dmi_info",dmitable["dmi_info"],nil)
--]]

-- [[
-- dumptable("dmitable.system",dmitable.system,nil)
-- dumptable("dmitable.bios",dmitable.bios,nil)
--dumptable("dmitable.chassis",dmitable.chassis,nil)
--]]

print(arg[1].." "..cmdline(2))
x = sl.get_key(600000)

sl.boot_linux(arg[1],cmdline(2))

-- Return to vesamenu
sl.run_command("vesamenu")
