#!/bin/bash
#
# Make an example into a given (non-existent) directory $1

set -e

if [ -z "$http_proxy" ] ; then
    read X -p "Interrupt with ^C to set http_proxy, or push enter to continue:"
fi

mkdir "$1"
cp *.sh *.cfg splash.png "$1"
cd "$1"
./mkit.sh
exec ./vm.sh
